/**
 * 
 */
package org.colin.ner;

import java.util.Properties;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.sequences.SeqClassifierFlags;
import edu.stanford.nlp.util.StringUtils;

/**
 * @author field
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//trainAndWrite("out.gz", "sample.properties", null);
		doTagging( getModel("./conll.distsim.iob2.crf.ser.gz"), "In the Kirby Institute's lab in Sydney's eastern suburbs, there's a freezer full of variants. From the original Wuhan strain, to Beta, Delta, and others in between, it's a record of how the COVID-19 virus has adapted and changed multiple times since it was first detected almost two years ago.\r\n"
				+ "\r\n"
				+ "This freezer of samples is now being put to good use as Australian scientists work around the clock to figure out exactly how the newest variant, Omicron, differs from those that came before.\r\n"
				+ "\r\n"
				+ "Perhaps the biggest question is how the new variant will fare against existing vaccines and natural antibodies, developed through previous exposure to the virus.\r\n"
				+ "\r\n"
				+ "Three scientists wearing full PPE analyse samples in a lab. \r\n"
				+ "A team of virologists from the Kirby Institute have been working non-stop since they received an Omicron sample. (Supplied: Stuart Turville)\r\n"
				+ "It's a question virologist Dr Stuart Turville and his team at the Kirby Institute are hoping to answer. Since they got their hands on an Omicron sample at the end of last month, the team hasn't left the lab.\r\n"
				+ "\r\n"
				+ "And while there is still plenty of work left to do, he says one thing has become clear this week: \"This thing is the most evasive one we've seen so far.\"\r\n"
				+ "\r\n"
				+ "What are we dealing with?\r\n"
				+ "The first stage of Turville and his team's investigations involved pitting the variant against 14 donor samples which had received two doses of either Pfizer or AstraZeneca and a product called hyperimmune globulin, made up of thousands of blood donors, which is used as an \"early warning device of how a variant may evade antibodies\".\r\n"
				+ "\r\n"
				+ "\"[Hyperimmune globulin] has this really beautiful breadth, so it has the ability to bind to lots of different SARS-CoV-2 variants,\" Turville says. \"If you've got a variant that escapes that or reduces the efficacy or potency of that product, then you're moving into kind of new territory.\"\r\n"
				+ "\r\n"
				+ "Omicron may be Alpha and Delta's love child and it shows the virus is adapting\r\n"
				+ "A woman in a lab coat smiles as she picks up a sample jar\r\n"
				+ "As the world rushes to understand how Omicron will impact global control of COVID-19, new research suggests the virus may be changing shape again.\r\n"
				+ "\r\n"
				+ "Read more\r\n"
				+ "By Wednesday this week, the team had a \"snapshot\" of what we are dealing with. The two-dose vaccine antibodies used in the test couldn't block Omicron.\r\n"
				+ "\r\n"
				+ "The hyperimmune globulin was able to inhibit Omicron, but at a concentration 11 to 13 fold greater than what is needed to get the same result against the earlier Wuhan strain.\r\n"
				+ "\r\n"
				+ "\"Essentially what we learnt from that experiment was that 'oh, this thing is evasive',\" he says. \"It's not surprising that a lot of people who are vaccinated are getting it.\"\r\n"
				+ "\r\n"
				+ "The next stage, which began this week and will likely take about a fortnight, is testing the variant against hundreds of antibody samples, ranging from people who have been vaccinated, to people that have recovered from Delta, and people who have been fully vaccinated and recovered from COVID-19.\r\n"
				+ "\r\n"
				+ "\"We just line up donor after donor after donor,\" Turville says. \"We'll be upwards, hopefully, of hundreds next week.\"\r\n"
				+ "\r\n"
				+ "That will hopefully result in a clearer picture of what donor antibodies are able to block Omicron, and what's unique about them, compared to those that can't.");
	}

	public static void doTagging(CRFClassifier<CoreLabel> model, String input) {
		input = input.trim();
		System.out.println(input + " => "  +  model.classifyToString(input));
	}

	public static CRFClassifier<CoreLabel> getModel(String modelPath) {
		return CRFClassifier.getClassifierNoExceptions(modelPath);
	}

	public static void trainAndWrite(String modelOutPath, String prop, String trainingFilepath) {
		Properties props = StringUtils.propFileToProperties(prop);
		props.setProperty("serializeTo", modelOutPath);
		//if input use that, else use from properties file.
		if (trainingFilepath != null) {
			props.setProperty("trainFile", trainingFilepath);
		}
		SeqClassifierFlags flags = new SeqClassifierFlags(props);
		CRFClassifier<CoreLabel> crf = new CRFClassifier<>(flags);
		crf.train();
		crf.serializeClassifier(modelOutPath);
	}
}
