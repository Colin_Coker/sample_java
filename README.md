This follows the Stanford NER tutorial at  
https://medium.com/swlh/stanford-corenlp-training-your-own-custom-ner-tagger-8119cc7dfc06

You'll have to either build your own model with a full train.txt, or add a compiled version. 

I'm using one of these:  
http://nlp.stanford.edu/software/conll.closed.iob2.crf.ser.gz  
http://nlp.stanford.edu/software/conll.distsim.iob2.crf.ser.gz  

(Just drop them in the project root)  

The Data Turks links in the tutorial are broken, and I wasn't able to find a copy of their training file online, so it's just working on the small sample from the tutorial.

Remember to add the stanford-corenlp-4.3.2.jar to your classpath.